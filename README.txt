
----------------------------------
HIDE EMPTY VIEW MODULE
----------------------------------

When using contextual filters in view blocks sometimes you come across a block that
is rendering even though it does not have content. The markup renders but there is
no result data. This module proposes a solution.

This module provides a check for content in your view and templates to pair with
those checks that override the view module templates.
 - views-view.tpl.php
 - views-view-unformatted.tpl.php
 - views-view-list.tpl.php


FEATURES & BENEFITS
-------------------
 - You can still override these templates in your theme.
 - Enable and forget, no configuration needed.
 - Save yourself time from checking for empty fields in your view.
 - Works with No Results Behavior settings in your view.


SUPPORTED ROW DISPLAY FORMATS
-----------------------------
 - Unformatted list
 - HTML list


NOTICES
-------

If you are already using theme template overrides you will need to update you theme
templates to include the extra checks that are being used in these module templates.
You can review the templates inside the /templates folder of the module.
